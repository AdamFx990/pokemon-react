# Pokemon Peruser

Pokemon Peruser is a React/typescript frontend for <a href="https://pokeapi.co">PokeAPI</a>.

![homepage](screenies/2022-08-21-135503_1920x1080_scrot.png)


# Getting Started
This section will instruct you on setting up this SPA for first-time users/developers.

## Prerequistes
- npm
- node

You may also use yarn instead of NPM. However, the next section assumes you are using NPM.

## Instructions
To run the application in development mode, run: 
```
npm i -D
npm start
```

To build the application for release, run: `npm run build`. You may then open the generated `index.html` in the browser of your choosing.

To execute tests, run: `npm test`.

## Additional Dependencies
Some additional libraries have been utilsed in this project. This section details what packages are used, and what their purpose is.

- `eslint` for code formatting/warnings. Details can be found in this repository at `/.eslintrc.yml`.
- `Axios` is used to handle API calls in place of vanilla JS `fetch()`, due to its wide browser support and friendly syntax.
- `jest` is utilised for typescript unit tests.
- `@testing-library/react` is used for snapshot and component testing.


# Usage
This section will explain the functions of the application.

## Pokemon Peruser
Upon loading the application, you'll be greated by the homepage. Here you can:
- Filter pokemon by generation (left)
- Search Pokemon by name (top-left)
- Cycle through pages (top)
- Alter the page size (top-right) 
- View pokemon species cards (centre)

### Searching
- Click on Pokemon Peruser
- Set the page size (see Pagination for more information)
- Click in the Search field in the top-left of the main content pane.
- Start type in the name of the Pokemon Species you wish to search for. The results will update in realtime.
![search](screenies/2022-08-21-135527_1920x1080_scrot.png)

### Favouriting
This section will explain the various methods of favouriting a Pokemon. This will save your favourite Pokemon for future visits to the site.

#### From Pokemon Peruser
- Click on Pokemon Peruser
- Search for the Pokemon Species you wish the favourite
- Use the arrows flanking the Pokemon's name within the card to cycle through the different varients.
- Press the star icon in the top-left corner of the same card section (just above the sprites).
- The icon will change to a gold star. This indicates the Pokemon has been favourited.

#### From Pokemon Compare
From Pokemon compare, you can compare two Pokemon, but did you know you could also favourite them?
- Select a pokemon from either one/both of the two lists.
- Press the star icon in the top-left corner of the same card section (just above the sprites).
- The icon will change to a gold star. This indicates the Pokemon has been favourited.

### Pagination
In various places within the application, you'll see text flanked by two arrows. This is for cycling through paginated information.
- Click on the left arrow to go to the previous item. 
- Click on the right arrow to go to the next item.

NOTE: Clickable items will show a `pointer` cursor.

## Filtering by Generation
In the Pokemon Peruser, you can filter Pokemon by generation. Click the generation in the left-hand pane, and the results will be restricted to just the Species available in that generation. This feature does not work in Pokemon Compare.

## Compare Pokemon
Clicking the Pokemon Compare button on the top navigation pane will bring you to the following page.
Here you can select two pokemon from either of the two scrollable lists at the top of the content area. Once selected, the card with that Pokemon species will appear below this list.
![compare](screenies/2022-08-21-135553_1920x1080_scrot.png)


# Additional features
This section lists features that are currently unimplemented, but maybe added in the future.

- Stats graphs for each Pokemon
- A section detailing any moves both share
- Search for the two lists
- Colour-coding to indicate which Pokemon has the best stats
- The ability to compare more than two Pokemon
