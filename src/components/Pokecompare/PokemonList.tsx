import React, { FC } from "react";
import { INamedAPIResource } from "../../interfaces/CommonResources";
import { IPokemon } from "../../interfaces/Pokemon";
import PokemonRow from "./PokemonRow";

type PokemonListProps = {
  pokemons: INamedAPIResource[];
  selectedName: string | undefined;
  onSelect: (pokemon: IPokemon) => void;
};

const PokemonList: FC<PokemonListProps> = ({
  selectedName,
  pokemons,
  onSelect,
}) => {
  return (
    <div className="pokecompare-list">
      {pokemons.map((p, i) => (
        <PokemonRow
          key={i}
          name={p.name}
          url={p.url}
          onSelect={onSelect}
          isSelected={p.name === selectedName}
        />
      ))}
    </div>
  );
};

export default PokemonList;
