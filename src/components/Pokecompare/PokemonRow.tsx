import React, { FC } from 'react';
import { IPokemon } from '../../interfaces/Pokemon';
import { GetPokemon } from '../../services/PokemonDataService';
import { formatApiName } from '../../services/TextFormatService';

type PokemonRowProps = {
    url: string,
    name: string,
    isSelected: boolean,
    onSelect: (pokemon : IPokemon) => void
}

const PokemonRow : FC<PokemonRowProps> = ({ name, url, onSelect, isSelected }) => {
    const onClick = async () => {
        if (!url) {
            return;
        }
        const data = await GetPokemon(url);
        onSelect(data);
    };

    return (
        <div className={`pokecompare-row ${isSelected ? 'selected' : 'clickable'}`} onClick={onClick}>
            {formatApiName(name)}
        </div>
    );
};

export default PokemonRow;
