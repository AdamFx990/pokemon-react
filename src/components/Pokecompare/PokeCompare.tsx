import React, { FC, useEffect, useState } from 'react';
import { INamedAPIResource } from '../../interfaces/CommonResources';
import { IPokemon } from '../../interfaces/Pokemon';
import { GetPokemons } from '../../services/PokemonDataService';
import SpeciesCard from '../Pokemon/SpeciesCard';
import PokemonList from './PokemonList';

const PokeCompare : FC = () => {
    const [pokemons, setPokemons] = useState<INamedAPIResource[]>([]);
    const [selectedPokemonL, setSelectedPokemonL] = useState<IPokemon>();
    const [selectedPokemonR, setSelectedPokemonR] = useState<IPokemon>();

    useEffect(() => {
        const getData = async () => {
          try {
            const data =  await GetPokemons();
            setPokemons(data.results);
          } catch (err) {
            alert(`An error occurred while getting pokemon. Please check your network connection`);
          }
        };
        getData();
      }, []);

      /**
       * Fired when a pokemon pill is clicked in the left-hand list
       * @param pokemon the selected pokemon
       */
    const onLeftSelect = (pokemon : IPokemon) => {
      setSelectedPokemonL(pokemon);
    };

    /**
       * Fired when a pokemon pill is clicked in the right-hand list
       * @param pokemon the selected pokemon
       */
    const onRightSelect = (pokemon : IPokemon) => {
      setSelectedPokemonR(pokemon);
    };

    return (
        <div className='pokecompare'>
            <h1 className='span-all-columns flex centre full-width'>Pokemon Compare</h1>

            <PokemonList pokemons={pokemons} onSelect={onLeftSelect} selectedName={selectedPokemonL?.name} />
            <PokemonList pokemons={pokemons} onSelect={onRightSelect} selectedName={selectedPokemonR?.name} />

            {selectedPokemonL && <SpeciesCard url={selectedPokemonL.species.url} />}
            {selectedPokemonR && <SpeciesCard url={selectedPokemonR.species.url} />}
        </div>
    );
};

export default PokeCompare;
