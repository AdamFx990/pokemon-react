import React, { FC, useContext, useEffect, useState } from 'react';
import { GenerationsContext } from '../../contexts/GenerationsContext';
import { IGeneration } from '../../interfaces/Games';
import { GetGeneration } from '../../services/PokemonDataService';

type GenerationRowProps = {
    url: string;
}

const GenerationRow : FC<GenerationRowProps> = ({ url }) => {
    const gc = useContext(GenerationsContext);
    const [generation, setGeneration] = useState<IGeneration>();

    useEffect(() => {
        const getData = async () => {
            if (!url.length) {
                return;
            }
            const data = await GetGeneration(url);
            setGeneration(data);
        };

        getData();
    }, [url]);

    const localisedName = generation?.names.find((n) => n.language.name === 'en');

    const onClick = () => {
        if (!generation) {
            return;
        }
        if (generation.id === gc?.selectedGeneration?.id) {
            gc?.setSelectedGeneration(undefined);
            return;
        }
        gc?.setSelectedGeneration(generation);
    };

    return (
        <div id={generation?.name} className={gc?.selectedGeneration?.id === generation?.id ? 'selected-row' : 'row clickable'} onClick={onClick}>
            <span>{localisedName?.name}</span>
        </div>
    );
};

export default GenerationRow;
