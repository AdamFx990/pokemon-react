import React, { FC, useEffect, useState } from 'react';
import { INamedAPIResource } from '../../interfaces/CommonResources';
import { GetGenerations } from '../../services/PokemonDataService';
import GenerationRow from './GenerationRow';


const GenerationList : FC = () => {
    const [generations, setGenerations] = useState<INamedAPIResource[] | undefined>();

    useEffect(() => {
        const getData = async () => {
            if (generations?.length) {
                return;
            }
            const data = await GetGenerations();
            setGenerations(data.results);
        };

        getData();
    }, [generations?.length]);

    return (
        <div className='nav-bar'>
            {generations?.map((g, i) => <GenerationRow key={i} url={g.url} />)}
        </div>
    );
};

export default GenerationList;
