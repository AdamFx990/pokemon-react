import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import ContentArea from './ContentArea';

test('renders contentArea', () => {
  render(<ContentArea>Testing</ContentArea>);
  const contentArea = screen.getByText(/Testing/i);

  expect(contentArea).toBeInTheDocument();
  expect(contentArea).toContainHTML('Testing');
});

test('snapshot content area', () => {
  const cellRenderer = render(<ContentArea>Snapshot test</ContentArea>);
  const cellFragment = cellRenderer.asFragment();
  expect(cellFragment).toMatchSnapshot();
});
