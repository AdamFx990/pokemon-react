import React, { FC, ReactNode } from "react";

type CellProps = {
  children: ReactNode | undefined;
};

const Cell: FC<CellProps> = ({ children }) => {
  return (
    <div className="cell-border">
      <span>{children}</span>
    </div>
  );
};

export default Cell;
