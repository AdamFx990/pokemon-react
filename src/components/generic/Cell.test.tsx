import React from 'react';
import { render, screen } from '@testing-library/react';
import Cell from './Cell';
import { unmountComponentAtNode } from 'react-dom';


test('renders cell', () => {
  render(<Cell>Testing</Cell>);
  const cell = screen.getByText(/Testing/i);
  expect(cell).toBeInTheDocument();
  expect(cell).toContainHTML('<span>Testing</span>');
});

test('snapshot cell', () => {
  const cellRenderer = render(<Cell>Snapshot test</Cell>);
  const cellFragment = cellRenderer.asFragment();
  expect(cellFragment).toMatchSnapshot();
});
