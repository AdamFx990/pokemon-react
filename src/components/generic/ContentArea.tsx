import React, { FC, ReactNode } from 'react';

type ContentAreaProps = {
    children: ReactNode | ReactNode[] | undefined
}

const ContentArea : FC<ContentAreaProps> = ({ children }) => {
    return (
        <div id='content-area' className='content-area'>
            {children}
        </div>
    );
};

export default ContentArea;
