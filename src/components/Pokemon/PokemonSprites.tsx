import React, { FC, useState } from "react";
import { IPokemonSprites } from "../../interfaces/Pokemon";
import { ReactComponent as UpArrow } from "../../svgs/up-arrow.svg";
import { ReactComponent as Favourite } from "../../svgs/favourite.svg";
import { ReactComponent as Unfavourite } from "../../svgs/unfavourite.svg";

type PokemonSpritesProps = {
  name: string;
  sprites: IPokemonSprites | undefined;
};

type SpritePair = {
  name: string;
  srcL: string | undefined;
  srcR: string | undefined;
};

const favouriteSize = 50;

const PokemonSprites: FC<PokemonSpritesProps> = ({ name, sprites }) => {
  const [currentSprites, setCurrentSprites] = useState<number>(0);
  const [isFavourite, setIsFavourite] = useState<boolean>(
    !!localStorage.getItem(name)
  );

  const availableSprites = Array<SpritePair>();

  if (sprites?.front_default && sprites.back_default) {
    availableSprites.push({
      name: "Default",
      srcL: sprites.front_default,
      srcR: sprites.back_default,
    });
  }
  if (sprites?.front_female && sprites.back_female) {
    availableSprites.push({
      name: "Female",
      srcL: sprites.front_female,
      srcR: sprites.back_female,
    });
  }
  if (sprites?.front_shiny && sprites.back_shiny) {
    availableSprites.push({
      name: "Shiny",
      srcL: sprites.front_shiny,
      srcR: sprites.back_shiny,
    });
  }
  if (sprites?.front_shiny_female && sprites.back_shiny_female) {
    availableSprites.push({
      name: "Shiny Female",
      srcL: sprites.front_shiny_female,
      srcR: sprites.back_shiny_female,
    });
  }

  /**
   * click event handler for the next button.
   * @param {React.MouseEvent<SVGSVGElement>} event
   */
  const onNextClick = (event: React.MouseEvent<SVGSVGElement>) => {
    event.preventDefault();
    if (currentSprites >= availableSprites.length - 1) {
      setCurrentSprites(0);
      return;
    }
    setCurrentSprites(currentSprites + 1);
  };

  /**
   * click event handler for the previous button.
   * @param {React.MouseEvent<SVGSVGElement>} event
   */
  const onPrevClick = (event: React.MouseEvent<SVGSVGElement>) => {
    event.preventDefault();
    if (currentSprites - 1 < 0) {
      setCurrentSprites(availableSprites.length - 1);
      return;
    }
    setCurrentSprites(currentSprites - 1);
  };

  /**
   * click event handler for the favourite svg.
   * Persists between reloads.
   * @param {React.MouseEvent<SVGSVGElement>} event
   */
  const onFavouriteClick = () => {
    const val = localStorage.getItem(name);
    if (val === null) {
      localStorage.setItem(name, "true");
      setIsFavourite(true);
    } else {
      localStorage.removeItem(name);
      setIsFavourite(false);
    }
  };

  return availableSprites.length ? (
    <div className="grid-two-columns span-all-columns row">
      <div className="span-all-columns">
        <div
          style={{ width: favouriteSize, height: favouriteSize }}
          onClick={onFavouriteClick}
          className="clickable"
        >
          {isFavourite ? (
            <Unfavourite id={`${name}-unfavourite`} width={favouriteSize} height={favouriteSize} />
          ) : (
            <Favourite id={`${name}-favourite`} width={favouriteSize} height={favouriteSize} />
          )}
        </div>
      </div>
      <img
        id={`${name}-sprite-front`}
        src={availableSprites[currentSprites]?.srcL}
        alt="front"
        className="full-width"
      />

      <img
        id={`${name}-sprite-back`}
        src={availableSprites[currentSprites]?.srcR}
        alt="back"
        className="full-width"
      />

      <div className="flex space-between span-all-columns">
        <UpArrow
          id={`${name}-sprite-prev-btn`}
          className="clickable"
          transform="rotate(270)"
          height="1.5em"
          fill="#eedece"
          onClick={onPrevClick}
        />

        <span>{`${availableSprites[currentSprites]?.name} (${
          currentSprites + 1
        } / ${availableSprites.length})`}</span>

        <UpArrow
          id={`${name}-sprite-next-btn`}
          className="clickable"
          transform="rotate(90)"
          height="1.5em"
          fill="#eedece"
          onClick={onNextClick}
        />
      </div>
    </div>
  ) : null;
};

export default PokemonSprites;
