import React, { FC, useEffect, useState } from "react";
import { IPokemonSpecies } from "../../interfaces/Pokemon";
import { GetPokemonSpecie } from "../../services/PokemonDataService";
import PokemonDetails from "./PokemonDetails";
import { ReactComponent as UpArrow } from "../../svgs/up-arrow.svg";
import { formatApiName } from "../../services/TextFormatService";

type SpeciesCardProps = {
  url: string;
};

const SpeciesCard: FC<SpeciesCardProps> = ({ url }) => {
  const [species, setSpecies] = useState<IPokemonSpecies>();
  const [selectedPokemon, setSelectedPokemon] = useState<number>(0);

  useEffect(() => {
    const getData = async () => {
      if (!url.length) {
        return;
      }
      const data = await GetPokemonSpecie(url);
      setSpecies(data);
    };
    getData();
  }, [url]);

  /**
   * click event handler for the next button.
   * @param {React.MouseEvent<SVGSVGElement>} event
   */
  const onNextClick = (event: React.MouseEvent<SVGSVGElement>) => {
    event.preventDefault();
    if (selectedPokemon >= (species?.varieties.length ?? 0) - 1) {
      setSelectedPokemon(0);
      return;
    }
    setSelectedPokemon(selectedPokemon + 1);
  };

  /**
   * click event handler for the previous button.
   * @param {React.MouseEvent<SVGSVGElement>} event
   */
  const onPrevClick = (event: React.MouseEvent<SVGSVGElement>) => {
    event.preventDefault();
    if (selectedPokemon - 1 < 0) {
      setSelectedPokemon((species?.varieties.length ?? 0) - 1);
      return;
    }
    setSelectedPokemon(selectedPokemon - 1);
  };

  return (
    <div className="species-card">
      <div className="flex space-between span-all-columns row full-width">
        <UpArrow
          className="clickable"
          transform="rotate(270)"
          height="1.5em"
          fill="#eedece"
          onClick={onPrevClick}
        />

        <h2 className="centre-text">{formatApiName(`${species?.varieties[selectedPokemon]?.pokemon.name
          } (${selectedPokemon + 1} / ${species?.varieties.length})`)}</h2>

        <UpArrow
          className="clickable"
          transform="rotate(90)"
          height="1.5em"
          fill="#eedece"
          onClick={onNextClick}
        />
      </div>
      {species?.varieties[selectedPokemon]?.pokemon?.url && (
        <PokemonDetails url={species?.varieties[selectedPokemon].pokemon.url} />
      )}
    </div>
  );
};

export default SpeciesCard;
