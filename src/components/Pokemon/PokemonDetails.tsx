import React, { FC, useEffect, useState } from "react";
import { IPokemon } from "../../interfaces/Pokemon";
import { GetPokemon } from "../../services/PokemonDataService";
import PokemonMoves from "./PokemonMoves";
import PokemonSprites from "./PokemonSprites";
import PokemonStats from "./PokemonStats";

type PokemonDetailsProps = {
  url: string;
};

const PokemonDetails: FC<PokemonDetailsProps> = ({ url }) => {
  const [pokemon, setPokemon] = useState<IPokemon>();

  useEffect(() => {
    const getData = async () => {
      if (!url.length) {
        return;
      }
      const data = await GetPokemon(url);
      setPokemon(data);
    };

    getData();
  }, [url]);

  return (
    <>
      <PokemonSprites sprites={pokemon?.sprites} name={pokemon?.name ?? ''} />

      <PokemonStats pokemon={pokemon} />

      <PokemonMoves moves={pokemon?.moves ?? []} />
    </>
  );
};

export default PokemonDetails;
