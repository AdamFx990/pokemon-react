import React, { FC, useContext, useEffect, useState } from "react";
import { INamedAPIResource } from "../../interfaces/CommonResources";
import {
  GetPokemonSpecies
} from "../../services/PokemonDataService";
import SpeciesCard from "./SpeciesCard";
import { ReactComponent as UpArrow } from "../../svgs/up-arrow.svg";
import { GenerationsContext } from "../../contexts/GenerationsContext"

const PokemonGrid: FC = () => {
  const gc = useContext(GenerationsContext);
  const [species, setSpecies] = useState<INamedAPIResource[] | undefined>();
  const [pageNo, setPageNo] = useState<number>(1);
  const [pageSize, setPageSize] = useState<number>(10);
  const [searchTerm, setSearchTerm] = useState<string>('');

  useEffect(() => {
    const getData = async () => {
      // If a generation is selected, use that species list
      if (gc?.selectedGeneration?.pokemon_species.length) {
        setSpecies(gc?.selectedGeneration?.pokemon_species.slice(0));
        return;
      }
      try {
        const data =  await GetPokemonSpecies();
        setSpecies(data.results);
      } catch (err) {
        alert(`An error occurred while getting pokemon species. Please check your network connection`);
      }
    };
    getData();
  }, [gc?.selectedGeneration]);

  /**
   * click event handler for the next button.
   * @param {React.MouseEvent<SVGSVGElement>} event
   */
  const onNextClick = (event: React.MouseEvent<SVGSVGElement>) => {
    event.preventDefault();
    setPageNo(pageNo + 1);
  };

  /**
   * click event handler for the previous button.
   * @param {React.MouseEvent<SVGSVGElement>} event
   */
  const onPrevClick = (event: React.MouseEvent<SVGSVGElement>) => {
    event.preventDefault();
    setPageNo(pageNo - 1);
  };

  /**
   * onChange exent handler for the page size numerical input.
   * @param event 
   */
  const onPageSizeChange = (event: React.FormEvent<HTMLInputElement>) => {
    setPageSize(Number(event.currentTarget.value));
  };

  /**
   * onChange exent handler for the search text input.
   * @param event 
   */
  const onSearchChange = (event: React.FormEvent<HTMLInputElement>) => {
    const val = event.currentTarget.value;
    if (val.length) {
      setPageSize(99999);
    } else {
      setPageSize(10);
    }
    setSearchTerm(val);
  };

  const totalPages = Math.ceil((species?.length ?? 0) / pageSize);
  let filteredResults = species?.slice(0);
  if (searchTerm) {
    filteredResults = filteredResults?.filter((s) => s.name.toLowerCase().includes(searchTerm.toLowerCase()));
  }
  filteredResults = filteredResults?.slice(pageSize * (pageNo - 1), pageSize * pageNo);

  return (
    <div>
      <h1 className='span-all-columns flex centre full-width'>Pokemon Peruser</h1>
      <div className="flex space-between default-margin">
        <div className="default-margin">
          <span>Search</span>
          <input type="text" onChange={onSearchChange} value={searchTerm} />
        </div>

        {pageNo > 1 ? (
          <UpArrow
            className="clickable"
            transform="rotate(270)"
            height={75}
            fill="#eedece"
            onClick={onPrevClick}
          />
        ) : (
          <div />
        )}

        <span className="centre-text">{`Page ${pageNo} of ${totalPages}`}</span>

        {pageNo <= totalPages ? (
          <UpArrow
            className="clickable"
            transform="rotate(90)"
            height={75}
            fill="#eedece"
            onClick={onNextClick}
          />
        ) : (
          <div />
        )}

        <span className="centre-text">{(species?.length ?? 0)} results</span>

        {!searchTerm?.length && (
          <span className="centre-text">
            <input
              type="number"
              onChange={onPageSizeChange}
              size={3}
              value={pageSize}
            />{" "}
            per page
          </span>
        )}
      </div>

      <div className="pokemon-grid">
        {filteredResults?.map((p, i) => (
          <SpeciesCard key={i} url={p.url} />
        ))}
      </div>
    </div>
  );
};

export default PokemonGrid;
