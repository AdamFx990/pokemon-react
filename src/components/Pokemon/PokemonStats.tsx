import React, { FC, Fragment } from "react";
import { IPokemon } from "../../interfaces/Pokemon";
import { formatApiName } from "../../services/TextFormatService";
import Cell from "../generic/Cell";

type PokemonStatsProps = {
  pokemon: IPokemon | undefined;
};

const PokemonStats: FC<PokemonStatsProps> = ({ pokemon }) => {
  return (
    <div className="full-width row span-all-columns">
      <h3 className="centre-text span-all-columns">Statistics</h3>

      <div className="grid-two-columns full-width grid-border">
        {pokemon?.base_experience && (
          <>
            <Cell>Base Experience</Cell>
            <Cell>{`${pokemon?.base_experience}xp`}</Cell>
          </>
        )}

        {pokemon?.height && (
          <>
            <Cell>Height</Cell>
            <Cell>{`${pokemon?.height}cm`}</Cell>
          </>
        )}

        <Cell>Default</Cell>
        <Cell>{pokemon?.is_default ? "Yes" : "No"}</Cell>

        {pokemon?.stats.map((s, i) => (
          <Fragment key={i}>
            <Cell>{formatApiName(s.stat.name)}</Cell>
            <Cell>{s.base_stat}</Cell>
          </Fragment>
        ))}
      </div>
    </div>
  );
};

export default PokemonStats;
