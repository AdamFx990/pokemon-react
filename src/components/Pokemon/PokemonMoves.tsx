import React, { FC, useState } from 'react';
import { IPokemonMove } from '../../interfaces/Pokemon';
import MoveDetails from './MoveDetails';
import { ReactComponent as UpArrow } from '../../svgs/up-arrow.svg';

type PokemonMovesProps = {
    moves: IPokemonMove[]
}

const PokemonMoves: FC<PokemonMovesProps> = ({ moves }) => {
    const [currentMove, setCurrentMove] = useState<number>(0);

    /**
     * click event handler for the next button.
     * @param {React.MouseEvent<SVGSVGElement>} event
     */
    const onNextClick = (event: React.MouseEvent<SVGSVGElement>) => {
        event.preventDefault();
        if (currentMove >= moves.length - 1) {
            setCurrentMove(0);
            return;
        }
        setCurrentMove(currentMove + 1);
    };

    /**
     * click event handler for the previous button.
     * @param {React.MouseEvent<SVGSVGElement>} event
     */
    const onPrevClick = (event: React.MouseEvent<SVGSVGElement>) => {
        event.preventDefault();
        if (currentMove - 1 < 0) {
            setCurrentMove(moves?.length - 1);
            return;
        }
        setCurrentMove(currentMove - 1);
    };

    return (
        <div className='span-all-columns full-width row'>
                <h3 className='centre-text'>Moves</h3>

            <MoveDetails url={moves?.[currentMove]?.move.url} />

            <div className='flex space-between span-all-columns'>
                <UpArrow className='clickable' transform='rotate(270)' height='1.5em' fill='#eedece' onClick={onPrevClick} />

                <span>{`${moves[currentMove]?.move.name} (${currentMove + 1} / ${moves.length})`}</span>

                <UpArrow className='clickable' transform='rotate(90)' height='1.5em' fill='#eedece' onClick={onNextClick} />
            </div>
        </div>
    );
};

export default PokemonMoves;
