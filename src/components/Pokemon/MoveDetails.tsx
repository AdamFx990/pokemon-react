import React, { FC, useEffect, useState } from "react";
import { IMove } from "../../interfaces/Moves";
import { GetMove } from "../../services/PokemonDataService";
import Cell from "../generic/Cell";

type MoveDetailsProps = {
  url: string | undefined;
};

const MoveDetails: FC<MoveDetailsProps> = ({ url }) => {
  const [move, setMove] = useState<IMove>();

  useEffect(() => {
    const getData = async () => {
      if (!url?.length) {
        return;
      }
      const data = await GetMove(url);
      setMove(data);
    };

    getData();
  }, [url]);

  return (
    <div className="full-width row span-all-columns">
      <div className="grid-two-columns full-width grid-border">
        {move?.accuracy && (
          <>
            <Cell>Accuracy</Cell>
            <Cell>{move?.accuracy}</Cell>
          </>
        )}

        {move?.effect_chance && (
          <>
            <Cell>Effect Chance</Cell>
            <Cell>{move?.effect_chance}</Cell>
          </>
        )}

        {move?.power && (
          <>
            <Cell>Power</Cell>
            <Cell>{move?.power}</Cell>
          </>
        )}

        {move?.pp && (
          <>
            <Cell>PP</Cell>
            <Cell>{move?.pp}</Cell>
          </>
        )}
      </div>
    </div>
  );
};

export default MoveDetails;
