/**
 * Reformats the PokeAPI strings to a more human-friendly form.
 * @param str - input string
 * @returns formatted string
 */
export const formatApiName = (str: string): string => {
    const segments = str.split('-');

    let retVal = '';
    for (let i = 0; i < segments.length; i++) {
        let seg = segments[i];
        seg = seg[0].toUpperCase() + seg.substring(1);
        seg = seg.replace('-', ' ');
        if (i < (segments.length - 1)) {
            seg += ' ';
        }
        retVal += seg;
    }
    return retVal;
};