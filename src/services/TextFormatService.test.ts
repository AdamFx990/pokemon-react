import { formatApiName } from './TextFormatService';

it('Should return a long formatted string', () => {
    const [input, expectedOutput] = ['the-quick-brown-fox', 'The Quick Brown Fox'];

    const result = formatApiName(input);

    expect(result).toEqual(expectedOutput);
});

it('Should return a 1 word formatted string', () => {
    const [input, expectedOutput] = ['short', 'Short'];

    const result = formatApiName(input);

    expect(result).toEqual(expectedOutput);
});