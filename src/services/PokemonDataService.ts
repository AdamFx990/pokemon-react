import Axios from "axios";
import { apiEndpoint } from "../constants/endpoints";
import { INamedAPIResourceList } from "../interfaces/CommonResources";
import { IGeneration } from "../interfaces/Games";
import { IMove } from "../interfaces/Moves";
import { IPokemon, IPokemonSpecies } from "../interfaces/Pokemon";

export const GetGenerations = async (): Promise<INamedAPIResourceList> => {
  const response = await Axios.get(`${apiEndpoint}/generation/`);
  return response.data;
};

export const GetGeneration = async (url: string): Promise<IGeneration> => {
  const response = await Axios.get(url);
  return response.data;
};

export const GetPokemon = async (url: string): Promise<IPokemon> => {
  const response = await Axios.get(url);

  return response.data;
};

export const GetPokemons = async (): Promise<INamedAPIResourceList> => {
  const response = await Axios.get(`${apiEndpoint}/pokemon/`, { params: { limit: 99999 } });
  return response.data;
};

export const GetPokemonSpecie = async (url : string): Promise<IPokemonSpecies> => {
  const response = await Axios.get(url);
  return response.data;
};

export const GetPokemonSpecies = async (): Promise<INamedAPIResourceList> => {
  const response = await Axios.get(`${apiEndpoint}/pokemon-species/`, {
    params: { limit: 99999 },
  });
  return response.data;
};

export const GetMove = async (url: string): Promise<IMove> => {
  const response = await Axios.get(url);

  return response.data;
};
