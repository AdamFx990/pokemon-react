import { IDescription, IFlavourText, IName, INamedAPIResource, IVersionGameIndex } from "./CommonResources"

export interface IPokemonAbility {
    is_hidden: boolean,
    slot: number,
    ability: INamedAPIResource
}

export interface IPokemonHeldItemVersion {
    version: INamedAPIResource,
    rarity: number
}

export interface IPokemonHeldItem {
    item: INamedAPIResource,
    version_details: IPokemonHeldItemVersion[]
}

export interface IPokemonMoveVersion {
    move_learn_method: INamedAPIResource,
    version_group: INamedAPIResource,
    level_learned_at: number
}

export interface IPokemonMove {
    move: INamedAPIResource,
    version_group_details: IPokemonHeldItemVersion[]
}

export interface IPokemonSprites {
    front_default: string,
    front_shiny: string,
    front_female: string,
    front_shiny_female: string,
    back_default: string,
    back_shiny: string,
    back_female: string,
    back_shiny_female: string
}

export interface IPokemonStat {
    stat: INamedAPIResource,
    effort: number,
    base_stat: number
}

export interface IPokemonType {
    slot: number,
    type: INamedAPIResource
}

export interface IPokemonTypePast {
    generation: INamedAPIResource,
    types: INamedAPIResource[]
}

export interface IPokemon {
    id: number,
    name: string,
    base_experience: number,
    height: number,
    is_default: boolean,
    order: number,
    weight: number,
    abilities: IPokemonAbility[],
    forms: INamedAPIResource[],
    game_indicies: IVersionGameIndex[],
    held_items: IPokemonHeldItem[],
    location_area_encounters: string,
    moves: IPokemonMove[],
    past_types: IPokemonTypePast[],
    sprites: IPokemonSprites,
    species: INamedAPIResource,
    stats: IPokemonStat[],
    types: IPokemonType[]
}

export interface IPokemonSpeciesDexEntry {
    entry_number: number,
    pokedex: INamedAPIResource
}

export interface IPalParkEncounterArea {
    base_score: number,
    rate: number,
    area: INamedAPIResource
}

export interface IPokemonSpeciesVariety {
    is_default: boolean,
    pokemon: INamedAPIResource
}

export interface IPokemonSpecies {
    id: number,
    name: string,
    order: number,
    gender_rate: number,
    capture_rate: number,
    base_happiness: number,
    is_baby: boolean,
    is_legendary: boolean,
    is_mythical: boolean,
    hatch_counter: number,
    has_gender_differences: boolean,
    growth_rate: INamedAPIResource,
    pokedex_numbers: IPokemonSpeciesDexEntry[],
    egg_groups: INamedAPIResource[],
    color: INamedAPIResource,
    shape: INamedAPIResource,
    evolves_from_species: INamedAPIResource,
    evolution_chain: INamedAPIResource,
    habitat: INamedAPIResource,
    generation: INamedAPIResource,
    names: IName[],
    pal_park_encounters: IPalParkEncounterArea[],
    flavor_text_entries: IFlavourText,
    form_description: IDescription,
    varieties: IPokemonSpeciesVariety[]
}
