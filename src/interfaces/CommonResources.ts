export interface IAPIResource {
    url: string
}

export interface INamedAPIResource {
    name: string,
    url: string
}

export interface INamedAPIResourceList {
    count: number,
    next: string,
    previous: string,
    results: INamedAPIResource[]
}

export interface IDescription {
    description: string,
    language: INamedAPIResource
}

export interface IName {
    name: string,
    language: INamedAPIResource
}

export interface IVersionGameIndex {
    game_index: number,
    version: INamedAPIResource
}

export interface IFlavourText {
    flavor_text: string,
    language: INamedAPIResource,
    version: INamedAPIResource
}
