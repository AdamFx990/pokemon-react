import { IName, INamedAPIResource } from "./CommonResources";

export interface IGeneration {
    id: number,
    name: string,
    abilities: INamedAPIResource[],
    names: IName[],
    main_region: INamedAPIResource,
    moves: INamedAPIResource[],
    pokemon_species: INamedAPIResource[],
    types: INamedAPIResource[],
    version_groups: Array<INamedAPIResource>
}
