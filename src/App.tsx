import React from "react";
import "./App.css";
import ContentArea from "./components/generic/ContentArea";
import GenerationList from "./components/Generations/GenerationList";
import PokemonGrid from "./components/Pokemon/PokemonGrid";
import { GenerationProvider } from "./contexts/GenerationsContext";
import { Route, Routes } from "react-router-dom";
import PokeCompare from "./components/Pokecompare/PokeCompare";
import { useNavigate } from "react-router-dom";

function App() {
  const navigate = useNavigate();

  const onHomeClick = () => {
    navigate('/');
  };

  const onCompareClick = () => {
    navigate('/compare');
  };

  return (
      <div className="App">
        <div className="top-nav">
          <div className="clickable top-nav-btn" onClick={onHomeClick}>Pokemon Peruser</div>
          <div className="clickable top-nav-btn" onClick={onCompareClick}>Compare Pokemon</div>
        </div>

        <GenerationProvider>
          <GenerationList />

          <ContentArea>
            <Routes>
              <Route path="/" element={<PokemonGrid />} />
              <Route path="/compare" element={<PokeCompare />} />
            </Routes>
          </ContentArea>
        </GenerationProvider>
      </div>
  );
}

export default App;
