import React, { createContext, useState, FC, ReactNode } from 'react';
import { IGeneration } from '../interfaces/Games';

interface IGenerationContext {
    selectedGeneration: IGeneration | undefined,
    setSelectedGeneration: (generation: IGeneration | undefined) => void
}

type GenerationProvder = {
  children: ReactNode;
}

export const GenerationsContext = createContext<IGenerationContext | null>(null);

export const GenerationProvider: FC<GenerationProvder> = ({ children }) => {
  const [selectedGeneration, setSelectedGeneration] = useState<IGeneration | undefined>();

  return (
    <GenerationsContext.Provider
      value={{
        selectedGeneration,
        setSelectedGeneration
      }}
    >
      {children}
    </GenerationsContext.Provider>
  );
};
