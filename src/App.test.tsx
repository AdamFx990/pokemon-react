import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import App from './App';
import { BrowserRouter } from 'react-router-dom';

test('renders navbar', () => {
  render(<BrowserRouter><App /></BrowserRouter>);

  const compareLink = screen.getByText(/Compare Pokemon/i);

  expect(compareLink).toBeInTheDocument();
  expect(compareLink).not.toBeDisabled();
});

test('Click Pokemon compare', () => {
  render(<BrowserRouter><App /></BrowserRouter>);

  const compareBtn = screen.getByText(/Compare Pokemon/i);
  const fireSuccess = fireEvent.click(compareBtn);
  expect(fireSuccess).toBeTruthy();

  const title = screen.getByText(/Pokemon Compare/i);
  expect(title).toBeInTheDocument();
});
